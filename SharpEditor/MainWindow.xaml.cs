﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;

///Program Name: SharpEditor
///Author:       Jon Decher
///Date:         June 21, 2013
///Description:  Saves, openes and edits .txt, .cs and .rtf files.

namespace SharpEditor
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private bool isBold = false;
        private bool isItalic = false;
        private bool isUnderlined = false;

        public MainWindow()
        {

            InitializeComponent();

            rText.Focus();

            try
            {
                for (double i = 2; i <= 72; ++i)
                {
                    cmbFontSize.Items.Add(i);
                    ++i;
                }
            }
            catch (Exception mainEx) { Console.WriteLine(mainEx); }

        }

        //-------Menu Items------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Menu item makes a new page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TextRange txt = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                txt.Text = "";
                tab1.Header = "New";
                fileNameTextBlock.Text = "";
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// Menu item that saves information in the rtb.
        /// Will only open a file dialog if the file is new otherwise will save to the current file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveItem_Click(object sender, RoutedEventArgs e)
        {

            if (tab1.Header.ToString().Equals("New"))
            {
                try
                {
                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.DefaultExt = ".txt";
                    dlg.Filter = "TEXT Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|CS Files (*.cs)|*.cs";
                    if (dlg.ShowDialog() == true)
                    {
                        TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                        string filename = dlg.FileName;
                        FileStream file = new FileStream(filename, FileMode.Create);
                        t.Save(file, System.Windows.DataFormats.Text);
                        file.Close();
                        tab1.Header = System.IO.Path.GetFileName(filename);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }
            else
            {
                try
                {

                    TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                    string filename = tab1.Header.ToString();
                    FileStream file = new FileStream(filename, FileMode.Create);
                    t.Save(file, System.Windows.DataFormats.Text);
                    file.Close();
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }

        }

        /// <summary>
        /// Menu item that saves information in the rtb and allows the user to choose a name and extension for the document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAsItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".txt";
                dlg.Filter = "TEXT Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|CS Files (*.cs)|*.cs";
                if (dlg.ShowDialog() == true)
                {
                    TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                    string filename = dlg.FileName;
                    FileStream file = new FileStream(filename, FileMode.Create);
                    t.Save(file, System.Windows.DataFormats.Text);
                    file.Close();
                    tab1.Header = System.IO.Path.GetFileName(filename);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// Menu Item that opens only .txt, .rtf and .cs files and displays on the rtb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                dlg.Filter = "TEXT Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|CS Files (*.cs)|*.cs";
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                    string filename = dlg.FileName;
                    FileStream file = new FileStream(filename, FileMode.Open);
                    t.Load(file, System.Windows.DataFormats.Text);
                    file.Close();

                    fileNameTextBlock.Text = System.IO.Path.GetFileName(filename);

                }
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        private void PrintItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.PrintDialog dlg = new System.Windows.Controls.PrintDialog();
            if (dlg.ShowDialog() == true)
            {
                dlg.PrintDocument((((IDocumentPaginatorSource)rText.
                     Document).DocumentPaginator), "Text Editor Printing");
            }
        }

        /// <summary>
        /// Opens the print preview dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintPreviewItem_Click(object sender, RoutedEventArgs e)
        {
            PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();


            printPreviewDialog.ShowDialog();

        }

        /// <summary>
        /// Menu item that closes application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }


        /// <summary>
        /// applies bold to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void boldItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                if (!isBold)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontWeightProperty, FontWeights.Bold);
                    isBold = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                    isBold = false;
                }


            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// applies italic to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void italicItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isItalic)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Italic);
                    isItalic = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Normal);
                    isItalic = false;
                }

            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// applies underline to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void underlineItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isUnderlined)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.TextDecorationsProperty, TextDecorations.Underline);
                    isUnderlined = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Normal);
                    isUnderlined = false;
                }

            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// undoes last thing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void undoItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Undo();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// redoes last thing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void redoItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Redo();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// cuts selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cutItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Cut();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// copies selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void copyItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Copy();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// pastes whatever is on the clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pasteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Undo();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        //---------Buttons--------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Button that makes a new page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TextRange txt = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                txt.Text = "";
                tab1.Header = "New";
                fileNameTextBlock.Text = "";
                lineNumTextBlock.Text = "0";
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// button that saves information from the rtb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            if (tab1.Header.ToString().Equals("New"))
            {
                try
                {
                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.DefaultExt = ".txt";
                    dlg.Filter = "TEXT Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|CS Files (*.cs)|*.cs";
                    if (dlg.ShowDialog() == true)
                    {
                        TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                        string filename = dlg.FileName;
                        FileStream file = new FileStream(filename, FileMode.Create);
                        t.Save(file, System.Windows.DataFormats.Text);
                        file.Close();
                        tab1.Header = System.IO.Path.GetFileName(filename);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }
            else
            {
                try
                {
                    TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                    string filename = tab1.Header.ToString();
                    FileStream file = new FileStream(filename, FileMode.Create);
                    t.Save(file, System.Windows.DataFormats.Text);
                    file.Close();
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }
        }

        /// <summary>
        /// button that will open only .txt, .rtf and .cs files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                dlg.Filter = "TEXT Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|CS Files (*.cs)|*.cs";
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    TextRange t = new TextRange(rText.Document.ContentStart, rText.Document.ContentEnd);
                    string filename = dlg.FileName;
                    FileStream file = new FileStream(filename, FileMode.Open);
                    t.Load(file, System.Windows.DataFormats.Text);
                    file.Close();
                    fileNameTextBlock.Text = "Extension: *" + System.IO.Path.GetExtension(filename);
                    tab1.Header = System.IO.Path.GetFileName(filename);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex); }


        }

        /// <summary>
        /// opens the print dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrint_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.PrintDialog dlg = new System.Windows.Controls.PrintDialog();
            if (dlg.ShowDialog() == true)
            {
                dlg.PrintDocument((((IDocumentPaginatorSource)rText.
                     Document).DocumentPaginator), "Text Editor Printing");
            }

        }
        /// <summary>
        /// applies bold to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBold_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isBold)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontWeightProperty, FontWeights.Bold);
                    isBold = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                    isBold = false;
                }

            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// applies italic to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonItalic_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isItalic)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Italic);
                    isItalic = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Normal);
                    isItalic = false;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// applies underline to the selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUnderline_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isUnderlined)
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.TextDecorationsProperty, TextDecorations.Underline);
                    isUnderlined = true;
                }
                else
                {
                    rText.Selection.ApplyPropertyValue(TextBlock.FontStyleProperty, FontStyles.Normal);
                    isUnderlined = false;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// cuts selected text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Cut();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// pastes from the clipboard onto the richtextbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPaste_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Paste();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// copies selected text 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCopy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Copy();
            }
            catch (Exception ex) { Console.WriteLine(ex); }

        }

        /// <summary>
        /// undoes last thing done
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUndo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Undo();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        /// <summary>
        /// redoes last thing done
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRedo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rText.Redo();
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }

        //--------The RTB------------------------------------------------------------------------------------

        /// <summary>
        /// updates the cmbFontSize to highlighted font size in the rtb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbFontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbFontSize.SelectedItem != null && !rText.Selection.IsEmpty)
                    rText.Selection.ApplyPropertyValue(TextBlock.FontSizeProperty, cmbFontSize.SelectedItem);
            }
            catch (Exception ex) { Console.WriteLine(ex); }

        }

        /// <summary>
        /// will update the status bar showing: current line number and the current extension of the file that is opened
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rText_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbFontSize.SelectedValue = (double)rText.Selection.GetPropertyValue(TextBlock.FontSizeProperty);

                TextPointer lineStart = rText.CaretPosition.GetLineStartPosition(0);
                TextPointer p = rText.Document.ContentStart.GetLineStartPosition(0);

                int lineNumber = 1;
                while (true)
                {
                    if (lineStart.CompareTo(p) < 0)
                        break;

                    int result;
                    p = p.GetLineStartPosition(1, out result);

                    if (result == 0)
                        break;

                    lineNumber++;
                }

                lineNumTextBlock.Text = lineNumber.ToString();
            }
            catch (Exception ex) { Console.WriteLine(ex); }

        }


    }

}